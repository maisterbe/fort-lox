# Require any additional compass plugins here.

# Set this to the root of your project when deployed:
http_path = "/"
css_dir = "web/assets/css"
sass_dir = "web/assets/sass"
images_dir = "web/assets/images"
javascripts_dir = "web/assets/js"
fonts_dir = "web/assets/fonts"

Sass::Script::Number.precision = 3

output_style = :compressed # :nested
environment = :production # :development

# To enable relative paths to assets via compass helper functions. Uncomment:
relative_assets = true

line_comments = false
color_output = false

retina_ext = File.join(File.dirname(__FILE__), 'retina')
require File.join(retina_ext, 'lib', 'sass_extensions.rb')
add_import_path File.join(retina_ext, 'stylesheets')

# If you prefer the indented syntax, you might want to regenerate this
# project again passing --syntax sass, or you can uncomment this:
# preferred_syntax = :sass
# and then run:
# sass-convert -R --from scss --to sass assets/sass scss && rm -rf sass && mv scss sass
