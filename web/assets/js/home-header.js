{

    var height;
    var $spacer;
    var $slideOutNav;

    function getHeroHeight() {

        return document.querySelector('.hero-spacer').getBoundingClientRect().height;

    }

    function handlewindowScroll() {

        var $nav = document.querySelector('.main-navigation-bar');

        if(height < window.pageYOffset) {
            
            if(!$nav.classList.contains('fixed-nav-bar')) {
                $nav.classList.add('fixed-nav-bar');
                spacer.style.display = 'block';
                $slideOutNav.classList.remove('hero-nav-height');
            }

        } else {
            if($nav.classList.contains('fixed-nav-bar')) {
                $nav.classList.remove('fixed-nav-bar');
                spacer.style.display = 'none';
                $slideOutNav.classList.add('hero-nav-height');
            }
        }
    }

    function init() {
        // get the height that needs to be scrolled
        height = getHeroHeight();
        
        // hide the spacer (at this moment the navbar is not fixed)
        spacer = document.querySelector('.main-navigation-bar-spacer');
        spacer.style.display = 'none';

        // set the menu to be full height (at this point there is no fixed navbar)
        $slideOutNav = document.querySelector('.nav-slide-out-container');
        $slideOutNav.classList.add('hero-nav-height');

        // Add scroll listener
        window.addEventListener('scroll', handlewindowScroll);

    }

    init()

}