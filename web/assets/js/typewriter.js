{

    var $typewriters;

    function animateText($elem, string, count, framesPast) {

        $elem.innerText = string.substring(0, count);

        if(count < string.length) {
                framesPast++;

                if(framesPast % 2 == 0) {
                    count++;
                    framesPast = 0;
                }

                window.requestAnimationFrame(function(){
                    animateText($elem, string, count, framesPast);
                });
        } else {
            $elem.parentElement.classList.add('typewritten-js');
            $elem.parentElement.classList.remove('typewriting-js');
        }

    }
    
    function write($elem) {

        var originalText = $elem.innerText;
        $elem.style.minHeight = $elem.getBoundingClientRect().height + 'px';
        animateText($elem, originalText, 0, 0);

    }

    function checkIfVisisble($elem) {

        var windowHeight = (window.innerHeight || document.documentElement.clientHeight);
        
        state = false

        if($elem.getBoundingClientRect().top >= 0 && $elem.getBoundingClientRect().top + ($elem.getBoundingClientRect().height / 2) <= windowHeight) {
            state = true;
        }

        return state;

    }

    function typewriter() {
        for(var i = 0; i < $typewriters.length; i++) {
            var $elem = $typewriters[i].children[0]
            if(checkIfVisisble($elem) && !$elem.parentElement.classList.contains('typewritten-js') && !$elem.parentElement.classList.contains('typewriting-js')) {
                $elem.parentElement.classList.remove('tobe-written-js');
                $elem.parentElement.classList.add('typewriting-js');
                write($elem);
            }
        }
    }

    function handleWindowScroll() {
        typewriter();
    }

    function init() {

        $typewriters = document.querySelectorAll('.typewriter-js');

        for(var i = 0; i < $typewriters.length; i++) {

            $typewriters[i].classList.add('tobe-written-js');
        }

        window.addEventListener('scroll', handleWindowScroll);

        typewriter();
        
    }

    init();

}