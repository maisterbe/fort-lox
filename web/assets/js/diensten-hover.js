{

    var $diensten;

    function setupMinHeights($elems) {

        for(var i  = 0; i < $elems.length; i++) {
            var height = $elems[i].getBoundingClientRect().height;
            $elems[i].style.minHeight = height + 'px';
        }

    }

    function setupButtonPositions($elems) {
        for(var i  = 0; i < $elems.length; i++) {
            
            var btn = $elems[i].querySelector('.btn-container');
            
        }
    }

    function init() {

        $diensten = document.querySelectorAll('.home-diensten .dienst');
        setupMinHeights($diensten);
        setupButtonPositions($diensten);

    }

    init();
}