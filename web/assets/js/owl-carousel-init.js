$(document).ready(function(){
    $(".owl-1").owlCarousel({
        items: 4,
        loop: true,
        nav: true,
        autoplay: true,
        autoplayTimeout: 4000,
        navText: ['<','>'],
        navContainer: '#partners-owl-custom-controls',
        responsive: {
            0: {
                items: 1
            },

            600 : {
                items: 2
            },

            800 : {
                items: 3
            },

            1200 : {
                items: 4
            }
        }
    });

    $(".verhalen-owl").owlCarousel({
        items: 3,
        margin: 15,
        nav: true,
        loop: true,
        autoplay: true,
        autoplayTimeout: 4000,
        navContainer: '#verhalen-owl-nav',
        navText: ['<','>'],
        responsive: {
            0: {
                items: 1
            },

            800 : {
                items: 2
            },

            1200 : {
                items: 3
            }
        }
    });

    $(".verhaal-images-slider").owlCarousel({
        items: 5,
        margin: 15,
        nav: true,
        navContainer: '#verhaal-images-owl-nav',
        navText: ['<','>'],
        autoWidth: true,
        loop: false
    });

    
  });