mapboxgl.accessToken = 'pk.eyJ1IjoibWFpc3RlcmJlIiwiYSI6ImNrMjMwa2ZteDF1NDIzb3F0MzhreGVsOTAifQ.zRqMzN2LDWFG_3F3Qi0nJQ';
var map = new mapboxgl.Map({
    container: 'map',
    style: 'mapbox://styles/maisterbe/ck230nl7l02nb1ckvk3lfx7ck',
    center: [3.192335, 51.0263],
    zoom: 8
});

map.on("load", function() {
    map.loadImage(
      "/assets/img/lock-icon.png",
      function(error, image) {
        if (error) throw error;
        map.addImage("lock", image);
        map.addLayer({
          id: "points",
          type: "symbol",
          source: {
            type: "geojson",
            data: {
              type: "FeatureCollection",
              features: [
                {
                  type: "Feature",
                  geometry: {
                    type: "Point",
                    coordinates: [3.2265454, 51.1121925]
                  }
                },
                {
                  type: "Feature",
                  geometry: {
                    type: "Point",
                    coordinates: [3.1581243, 50.9404584]
                  }
                }
              ]
            }
          },
          layout: {
            "icon-image": "lock",
            "icon-size": .5
          }
        });
      }
    );
  });

