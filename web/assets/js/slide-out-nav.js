{

    var nav = '';
    var $triggers;

    function handleMenuClicked(e) {
        
        $nav = document.querySelector('.nav-slide-out-container');
        $triggers = document.querySelectorAll('.menu-link-container .trigger');

        if($nav.classList.contains('slide-out')) {
            $nav.classList.remove('slide-out');
        } else {
            $nav.classList.add('slide-out');

            $linksInNav = $nav.querySelectorAll('a');

            for(var i  = 0; i < $linksInNav.length; i++) {
                $linksInNav[i];
                $linksInNav[i].addEventListener('click', handleLinkInMenuClicked);
            }
            

        }

        icon();

    }

    function icon() {
        for(var i = 0; i < $triggers.length; i++) {
            if($triggers[i].classList.contains('menu-opened')) {
                $triggers[i].classList.remove('menu-opened');
            } else {
                $triggers[i].classList.add('menu-opened');
            }
        }
    }

    function handleLinkInMenuClicked() {
        
        if($nav.classList.contains('slide-out')) {
            $nav.classList.remove('slide-out');
        }

        icon();

    }

    function init() {
        var $triggers = document.querySelectorAll('.menu-link-container .trigger');

        for(var i = 0; i < $triggers.length; i++) {
            $triggers[i].addEventListener('click', handleMenuClicked);
        }

    }

    init();

}