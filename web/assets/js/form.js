{

    function parseURLParams(url) {
        var queryStart = url.indexOf("?") + 1,
            queryEnd   = url.indexOf("#") + 1 || url.length + 1,
            query = url.slice(queryStart, queryEnd - 1),
            pairs = query.replace(/\+/g, " ").split("&"),
            parms = {}, i, n, v, nv;
    
        if (query === url || query === "") return;
    
        for (i = 0; i < pairs.length; i++) {
            nv = pairs[i].split("=", 2);
            n = decodeURIComponent(nv[0]);
            v = decodeURIComponent(nv[1]);
    
            if (!parms.hasOwnProperty(n)) parms[n] = [];
            parms[n].push(nv.length === 2 ? v : null);
        }
        return parms;
    }

    function showThankyouMessage() {

        var $div = document.createElement('div');
        $div.classList.add('thank-you-msg');

        var $p = document.createElement('p');
        $p.innerText = 'Bedankt voor uw bericht.';

        $div.appendChild($p);

        document.querySelector('body').appendChild($div);

        setTimeout(function() {
            $div.remove();
        }, 3000)

    }

    function fillInSubjet(subject) {
        var $input = document.querySelector('.input-block .subject');
        $input.value = subject;
    }

    function init() {
        if (parseURLParams(window.location.href)['message'] != undefined && parseURLParams(window.location.href)['message'][0] == 'sent') {
            showThankyouMessage();
        }

        if(parseURLParams(window.location.href)['moreinfo'] != undefined && parseURLParams(window.location.href)['moreinfo'][0]) {
            fillInSubjet(parseURLParams(window.location.href)['moreinfo'][0]);
        }
    }

    init();

}