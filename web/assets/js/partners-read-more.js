{

    function handleReadMoreClicked(e) {
        
        var $textContainer = e.currentTarget.parentElement.parentElement.querySelector('.text-container');
        var $originalText = $textContainer.querySelector('.original-text');
        var $fullText = $textContainer.querySelector('.full-text');

        if(e.currentTarget.classList.contains('open')) {
            $originalText.style.display = 'none';
            $fullText.style.display = 'block';

            e.currentTarget.innerText = 'Lees minder >'

            e.currentTarget.classList.remove('open');
            e.currentTarget.classList.add('close');
        } else {
            $originalText.style.display = 'block';
            $fullText.style.display = 'none';

            e.currentTarget.innerText = 'Lees meer >'

            e.currentTarget.classList.remove('close');
            e.currentTarget.classList.add('open');
        }
    }

    function createFullText($text) {
        var $parent = $text.parentElement;
        var $newText = $text.cloneNode();
        $newText.innerText = $text.innerText;
        $newText.classList.add('full-text');
        $newText.style.display = 'none';
        $parent.appendChild($newText);
    }

    function createReadMoreBtn($text) {

        var $btnsContainer = $text.parentElement.parentElement.querySelector('.btns-container');

        var $trigger = document.createElement('a');
        $trigger.classList.add('read-more-trigger');
        $trigger.classList.add('open')
        $trigger.addEventListener('click', handleReadMoreClicked);
        $trigger.style.display = 'block';
        $trigger.style.cursor = 'pointer'
        $trigger.innerText = 'Lees meer >'
        
        console.log($trigger);
        
        $btnsContainer.insertBefore($trigger, $btnsContainer.firstChild);

    }

    function shortenText($text) {
        
        var text = $text.innerText;
        var newText = text.substr(0, 150);
        newText = newText + '...';

        $text.innerText = newText;
    }

    function setUpReadMore ($text) {

        createFullText($text);
        createReadMoreBtn($text);
        shortenText($text);


    }

    function CheckTexts($texts) {
        for(var i = 0; i < $texts.length; i++) {
            var $text = $texts[i];

            if($text.innerText.length > 150) {
                setUpReadMore($text);
            }
        }
    }

    function init() {
        
        var $texts = document.querySelectorAll('.partner-container .text-container p');

        CheckTexts($texts);

    }

    init();

}